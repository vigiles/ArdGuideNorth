# ArdGuideNorth 
 ![github](ic_launcher-web.png "安卓开发指北")
<br/>
## 概述
&emsp;《安卓开发指北》apk项目，尽量集成业界内最新技术，为android开发小白指点一二。
<br/>
&emsp;网页版地址 [http://www.gaohaiyan.com/guide_north/](http://www.gaohaiyan.com/guide_north/)
<br/><br/>

## 功能设计
<li><b>博文展示</b>。基本功能，提供技术文档显示<br/>
<li><b>收藏</b>。对有意向的文档做过记录<br/>
<li><b>在线更新</b>。检测是不是有最新文档发布<br/>
<li><b>错误反馈</b>。难免百密一疏，向作者反馈避免误导用户<br/>
<li><b>设置</b>。对应用进行必要设置。如wifi下更新，检测版本等<br/>
<li><b>用户登录</b>。登录后可以同步收藏到服务器，及以下操作<br/>
<li><b>博文点评</b>。登录后点评查看的文档，可以进行讨论<br/>
<li><b>投稿</b>。登录后可以作为提供内容的参与者<br/>
<li><b>技术求新</b>。哪个技术不懂了登录后可以索取之<br/>
<br/>

## 可视模块设计
<li><b>欢迎</b> 欢迎界面中进行新版本检测，根据需要更新apk<br/>
<li><b>登录</b> 验证用户信息，同步应用的配置数据<br/>
<li><b>展示</b> 提供文档列表，显示主页<br/>
<li><b>设置</b> 对一些必要参数进行设置<br/>
<li><b>收藏</b> 保存用户的收藏文档<br/>
<li><b>讨论</b> 对技术文档的讨论 <br/>
<li><b>反馈</b> 对本应用的使用建议意见等<br/>
<br/>

## 流程设计
图
<br/><br/>

## 进展
设计中
<br/><br/>

## 作者
<li>vigiles
<li>vigiles@163.com
<br/>